/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package input

import (
	"encoding/binary"
	"errors"
	"log"

	"gitlab.gnome.org/wikylyu/galaxy/cipher"
	"gitlab.gnome.org/wikylyu/galaxy/gnet"
	"gitlab.gnome.org/wikylyu/galaxy/protocol/ss"
	"gitlab.gnome.org/wikylyu/galaxy/tunnel/agent"
	"gitlab.gnome.org/wikylyu/galaxy/tunnel/agent/util"
)

type ShadowsocksFactory struct {
	method     string
	password   string
	cipherInfo *cipher.CipherInfo
}

func (*ShadowsocksFactory) Name() string {
	return "shadowsocks"
}

func (f *ShadowsocksFactory) Setup(options agent.Options) error {
	f.method = options.GetString("method")
	f.password = options.GetString("password")
	f.cipherInfo = cipher.GetCipherInfo(f.method)
	if f.cipherInfo == nil {
		return errors.New("invalid method")
	} else if f.password == "" {
		return errors.New("invalid password")
	}
	return nil
}

func (f *ShadowsocksFactory) Options() map[string]string {
	return map[string]string{
		"method":   f.method,
		"password": f.password,
	}
}

func (f *ShadowsocksFactory) NewIAgent(gc *gnet.GConn) (agent.IAgent, error) {
	iv := cipher.RandKey(f.cipherInfo.IvSize)
	key := ss.KDF(f.password, f.cipherInfo.KeySize)
	subkey := ss.HKDFSHA1(key, f.cipherInfo.KeySize, iv)

	ekey := key
	if f.cipherInfo.Block {
		ekey = subkey
	}
	encrypter, err := f.cipherInfo.EncrypterFunc(ekey, iv)
	if err != nil {
		log.Printf("failed to create encrypter:%v", err)
		return nil, err
	}
	return &ShadowsocksAgent{
		GConn:      gc,
		method:     f.method,
		password:   f.password,
		cipherInfo: f.cipherInfo,
		encrypter:  encrypter,
		decrypter:  nil,
		key:        key,
		subkey:     subkey,
		iv:         iv,
		ivSent:     false,
		buf:        nil,
	}, nil
}

type ShadowsocksAgent struct {
	*gnet.GConn
	method     string
	password   string
	cipherInfo *cipher.CipherInfo
	encrypter  cipher.Encrypter
	decrypter  cipher.Decrypter
	key        []byte
	subkey     []byte
	iv         []byte
	buf        []byte
	ivSent     bool
	request    *ss.AddressRequest
}

func (a *ShadowsocksAgent) Init() (string, uint16, error) {
	if a.decrypter != nil {
		return "", 0, nil
	}
	ivbuf, err := a.ReadFull(a.cipherInfo.IvSize)
	if err != nil {
		return "", 0, err
	}
	ekey := a.key
	if a.cipherInfo.Block {
		ekey = ss.HKDFSHA1(a.key, a.cipherInfo.KeySize, ivbuf)
	}
	decrypter, err := a.cipherInfo.DecrypterFunc(ekey, ivbuf)
	if err != nil {
		return "", 0, err
	}
	a.decrypter = decrypter
	data, err := a.Read()
	if err != nil {
		return "", 0, err
	}
	req, err := ss.ParseAddressRequest(data)
	if err != nil {
		return "", 0, err
	}
	a.buf = req.BUF
	a.request = req
	return req.ADDR, req.PORT, nil
}

func (a *ShadowsocksAgent) Notify(err error) error {
	return err
}

func (a *ShadowsocksAgent) readBlock() ([]byte, error) {
	lenbuf, err := a.ReadFull(2 + a.cipherInfo.TagSize)
	if err != nil {
		return nil, err
	}
	lenbytes := a.decrypter.Decrypt(lenbuf)
	if lenbytes == nil {
		return nil, ss.ErrInvalidMessage
	}
	length := util.BytesToUint16(lenbytes, binary.BigEndian)
	if length == 0 {
		return nil, ss.ErrInvalidMessage
	}
	databuf, err := a.ReadFull(int(length) + a.cipherInfo.TagSize)
	if err != nil {
		return nil, err
	}
	return a.decrypter.Decrypt(databuf), nil
}

func (a *ShadowsocksAgent) readStream() ([]byte, error) {
	if len(a.buf) > 0 {
		buf := a.buf
		a.buf = nil
		return buf, nil
	}

	if data, err := a.GConn.Read(); err != nil {
		return nil, err
	} else {
		return a.decrypter.Decrypt(data), nil
	}
}

func (a *ShadowsocksAgent) Read() ([]byte, error) {
	if a.cipherInfo.Block {
		return a.readBlock()
	} else {
		return a.readStream()
	}
}

func (a *ShadowsocksAgent) Write(data []byte) error {
	if !a.ivSent {
		a.GConn.Write(a.iv)
		a.ivSent = true
	}
	if a.cipherInfo.Block {
		a.GConn.Write(a.encrypter.Encrypt(util.Uint16ToBytes(uint16(len(data)), binary.BigEndian)))
	}
	cipherdata := a.encrypter.Encrypt(data)
	return a.GConn.Write(cipherdata)
}

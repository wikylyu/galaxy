/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package agent

import (
	"fmt"
)

type Options map[string]interface{}

func (opt Options) GetString(key string) string {
	v := opt[key]
	if v == nil {
		return ""
	}
	switch val := v.(type) {
	case int, int8, uint8, int16, uint16, int32, uint32, int64, uint64:
		return fmt.Sprintf("%d", val)
	case float32, float64:
		return fmt.Sprintf("%.f", val)
	case string:
		return val
	}
	return ""
}

func (opt Options) GetInt(key string) int {
	v := opt[key]
	if v == nil {
		return 0
	}
	switch val := v.(type) {
	case int:
		return int(val)
	case int8:
		return int(val)
	case uint8:
		return int(val)
	case int16:
		return int(val)
	case uint16:
		return int(val)
	case int32:
		return int(val)
	case uint32:
		return int(val)
	case int64:
		return int(val)
	case uint64:
		return int(val)
	case float32:
		return int(val)
	case float64:
		return int(val)
	}
	return 0
}

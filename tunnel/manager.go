/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package tunnel

import (
	"sort"
	"sync"

	"gitlab.gnome.org/wikylyu/galaxy/tunnel/agent"

	glob "github.com/ryanuber/go-glob"
)

// Manager 代理管理器，管理所有代理
type Manager struct {
	sync.Mutex
	tunnels map[string]*Tunnel
}

func NewManager() *Manager {
	return &Manager{
		tunnels: make(map[string]*Tunnel),
	}
}

func (m *Manager) GetTunnel(name string) (*Tunnel, *Error) {
	t := m.tunnels[name]
	if t == nil {
		return nil, ErrTunnelNotFound
	}
	return t, nil
}

// AddTunnel 添加一个代理
func (m *Manager) AddTunnel(name string, addr string, port uint16, iOptions, oOptions agent.Options, maxConn int32) *Error {
	defer m.Unlock()
	m.Lock()

	if len(name) == 0 {
		return ErrTunnelNameInvalid
	}

	t := m.tunnels[name]
	if t != nil {
		return ErrTunnelNameExists
	}
	t, err := New(addr, port, iOptions, oOptions, maxConn)
	if err != nil {
		return err
	}
	t.Name = name
	m.tunnels[name] = t
	return nil
}

func (m *Manager) UpdateTunnel(name string, addr string, port uint16, iOptions, oOptions agent.Options, maxConn int32) *Error {
	defer m.Unlock()
	m.Lock()
	t := m.tunnels[name]
	if t == nil {
		return ErrTunnelNotFound
	}
	if t.Status == StatusRunning {
		return ErrTunnelRunning
	}
	return t.Update(addr, port, iOptions, oOptions, maxConn)
}

// StartTunnel 启动一个代理
func (m *Manager) StartTunnel(name string) *Error {
	defer m.Unlock()
	m.Lock()
	t := m.tunnels[name]
	if t == nil {
		return ErrTunnelNotFound
	}
	return t.Start()
}

// StopTunnel 关闭一个代理
func (m *Manager) StopTunnel(name string) *Error {
	defer m.Unlock()
	m.Lock()
	t := m.tunnels[name]
	if t == nil {
		return ErrTunnelNotFound
	}
	return t.Stop()
}

// RemoveTunnel 删除代理
func (m *Manager) RemoveTunnel(name string) *Error {
	defer m.Unlock()
	m.Lock()
	t := m.tunnels[name]
	if t == nil {
		return ErrTunnelNotFound
	}
	if t.Status == StatusRunning {
		return ErrTunnelRunning
	}
	delete(m.tunnels, name)
	return nil
}

func (m *Manager) FindTunnels(query string) []*Tunnel {
	defer m.Unlock()
	m.Lock()

	keys := make([]string, 0)
	tunnels := make([]*Tunnel, 0)

	for name := range m.tunnels {
		if glob.Glob(query, name) {
			keys = append(keys, name)
		}
	}
	sort.Strings(keys)
	for _, name := range keys {
		tunnels = append(tunnels, m.tunnels[name])
	}
	return tunnels
}

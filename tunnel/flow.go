/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */
package tunnel

import (
	"sync/atomic"
	"time"
)

/* 流量统计 */
type FlowStat struct {
	Year    int
	Month   int
	Day     int
	Hour    int
	Ingress uint64
	Egress  uint64
}

func (ts *FlowStat) I(i uint64) {
	atomic.AddUint64(&ts.Ingress, i)
}

func (ts *FlowStat) E(i uint64) {
	atomic.AddUint64(&ts.Egress, i)
}

func (t *Tunnel) currentFlowStat() *FlowStat {
	now := time.Now()
	year := now.Year()
	month := int(now.Month())
	day := now.Day()
	hour := now.Hour()

	defer t.Unlock()
	t.Lock()
	last := len(t.FlowStats) - 1
	var fs *FlowStat
	if last < 0 {
		fs = &FlowStat{
			Year:  year,
			Month: month,
			Day:   day,
			Hour:  hour,
		}
		t.FlowStats = append(t.FlowStats, fs)
	} else {
		fs = t.FlowStats[last]
		if fs.Year != year || fs.Month != month || fs.Day != day || fs.Hour != hour {
			fs = &FlowStat{
				Year:  year,
				Month: month,
				Day:   day,
				Hour:  hour,
			}
			t.FlowStats = append(t.FlowStats, fs)
		}
	}
	return fs
}


// IE 增加入口和出口流量的统计
func (t *Tunnel) IE(i, e uint64) {
	if i == 0 || e == 0 {
		return
	}
	fs := t.currentFlowStat()
	fs.I(i)
	fs.E(e)
}

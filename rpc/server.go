/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package rpc

import (
	"context"
	"encoding/json"

	"time"

	"gitlab.gnome.org/wikylyu/galaxy/rpc/pb"
	"gitlab.gnome.org/wikylyu/galaxy/tunnel"
)

type AppInfo struct {
	AppName    string
	AppVersion string
	AppBuild   string
	AppCommit  string
}

type GravityServer struct {
	info *AppInfo
	tm   *tunnel.Manager
}

func NewGravityServer(appName, appVersion, appBuild, appCommit string) *GravityServer {
	return &GravityServer{
		info: &AppInfo{
			AppName:    appName,
			AppVersion: appVersion,
			AppBuild:   appBuild,
			AppCommit:  appCommit,
		},
		tm: tunnel.NewManager(),
	}
}

func (s *GravityServer) addTunnel(name, addr string, port uint16, iOptions, oOptions map[string]interface{}, maxConn int32) *tunnel.Error {
	return s.tm.AddTunnel(name, addr, port, iOptions, oOptions, maxConn)
}

func (s *GravityServer) Version(context.Context, *pb.Void) (*pb.VersionReply, error) {
	return &pb.VersionReply{
		AppName:    s.info.AppName,
		AppVersion: s.info.AppVersion,
		AppBuild:   s.info.AppBuild,
		AppCommit:  s.info.AppCommit,
	}, nil
}

func (s *GravityServer) AddTunnel(ctx context.Context, req *pb.AddTunnelRequest) (*pb.CommonReply, error) {
	reply := &pb.CommonReply{
		Status: tunnel.StatusOK,
	}
	iOptions := make(map[string]interface{})
	oOptions := make(map[string]interface{})
	if err := json.Unmarshal([]byte(req.Input), &iOptions); err != nil {
		reply.Status = tunnel.StatusInputProtocolOptionInvalid
	} else if err := json.Unmarshal([]byte(req.Output), &oOptions); err != nil {
		reply.Status = tunnel.StatusOutputProtocolOptionInvalid
	} else if err := s.addTunnel(req.Name, req.Addr, uint16(req.Port), iOptions, oOptions, req.MaxConn); err != nil {
		reply.Status = err.Status
	}
	return reply, nil
}

func (s *GravityServer) UpdateTunnel(ctx context.Context, req *pb.UpdateTunnelRequest) (*pb.CommonReply, error) {
	reply := &pb.CommonReply{
		Status: tunnel.StatusOK,
	}

	var iOptions, oOptions map[string]interface{}
	if req.Input != "" {
		if err := json.Unmarshal([]byte(req.Input), &iOptions); err != nil {
			reply.Status = tunnel.StatusInputProtocolOptionInvalid
			return reply, nil
		}
	}
	if req.Output != "" {
		if err := json.Unmarshal([]byte(req.Output), &oOptions); err != nil {
			reply.Status = tunnel.StatusOutputProtocolOptionInvalid
			return reply, nil
		}
	}
	if err := s.tm.UpdateTunnel(req.Name, req.Addr, uint16(req.Port), iOptions, oOptions, req.MaxConn); err != nil {
		reply.Status = err.Status
	}
	return reply, nil
}

func (s *GravityServer) startTunnel(name string) *tunnel.Error {
	return s.tm.StartTunnel(name)
}

func (s *GravityServer) StartTunnel(ctx context.Context, req *pb.StartTunnelRequest) (*pb.CommonReply, error) {
	reply := &pb.CommonReply{
		Status: tunnel.StatusOK,
	}
	err := s.startTunnel(req.Name)
	if err != nil && !(err.Status == tunnel.StatusTunnelAlreadyRunning && req.IgnoreRunning) {
		reply.Status = err.Status
	}
	return reply, nil
}

func (s *GravityServer) StopTunnel(ctx context.Context, req *pb.StopTunnelRequest) (*pb.CommonReply, error) {
	reply := &pb.CommonReply{
		Status: tunnel.StatusOK,
	}
	err := s.tm.StopTunnel(req.Name)
	if err != nil && !(err.Status == tunnel.StatusTunnelNotRunning && req.IgnoreIdle) {
		reply.Status = err.Status
	}
	return reply, nil
}

func (s *GravityServer) RemoveTunnel(ctx context.Context, req *pb.RemoveTunnelRequest) (*pb.CommonReply, error) {
	reply := &pb.CommonReply{
		Status: tunnel.StatusOK,
	}
	err := s.tm.RemoveTunnel(req.Name)
	if err != nil {
		if err.Status == tunnel.StatusTunnelRunning && req.Force {
			reply.Status = err.Status
			if err := s.tm.StopTunnel(req.Name); err != nil {
				reply.Status = err.Status
			} else if err := s.tm.RemoveTunnel(req.Name); err != nil {
				reply.Status = err.Status
			}
		} else {
			reply.Status = err.Status
		}
	}
	return reply, nil
}

func (s *GravityServer) ListTunnel(ctx context.Context, req *pb.ListTunnelRequest) (*pb.ListTunnelReply, error) {
	reply := &pb.ListTunnelReply{
		Tunnels: make([]*pb.TunnelInfo, 0),
	}

	tunnels := s.tm.FindTunnels(req.Query)
	for _, t := range tunnels {
		ti := &pb.TunnelInfo{
			Name:      t.Name,
			Addr:      t.Addr,
			Port:      uint32(t.Port),
			Status:    pb.TunnelStatus(t.Status),
			IProtocol: t.IFactory.Name(),
			OProtocol: t.OFactory.Name(),
			MaxConn:   t.MaxConn,
			CurConn:   t.CurConn,
			StartTime: uint64(t.STime.UnixNano()),
		}
		if req.Options {
			ti.IOptions = t.IFactory.Options()
			ti.OOptions = t.OFactory.Options()
		}
		reply.Tunnels = append(reply.Tunnels, ti)
	}

	return reply, nil
}

func (s *GravityServer) StatTunnel(ctx context.Context, req *pb.StatTunnelRequest) (*pb.StatTunnelReply, error) {
	reply := &pb.StatTunnelReply{
		Status:    tunnel.StatusOK,
		FlowStats: make([]*pb.FlowStat, 0),
	}

	if t, err := s.tm.GetTunnel(req.Name); err != nil {
		reply.Status = err.Status
	} else {
		from, _ := time.Parse(time.RFC3339, req.From)
		to, err := time.Parse(time.RFC3339, req.To)
		if err != nil {
			to = time.Now().Add(time.Hour)
		}

		for _, fs := range t.FlowStats {
			fstime := time.Date(fs.Year, time.Month(fs.Month), fs.Day, fs.Hour, 0, 0, 0, time.Local)
			if (fstime.After(from) || fstime.Equal(from)) && (fstime.Before(to) || fstime.Equal(to)) {
				reply.FlowStats = append(reply.FlowStats, &pb.FlowStat{
					Year:    uint32(fs.Year),
					Month:   uint32(fs.Month),
					Day:     uint32(fs.Day),
					Hour:    uint32(fs.Hour),
					Ingress: fs.Ingress,
					Egress:  fs.Egress,
				})
			}
		}
	}

	return reply, nil
}

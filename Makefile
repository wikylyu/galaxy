
.PHONY:galaxy gravity

all: galaxy gravity

galaxy:
	@go build -ldflags "-X main.AppBuild=`date -u +%Y%m%d.%H%M%S` -X main.AppCommit=`git rev-list -1 HEAD`" -o bin/galaxy galaxy.go

gravity:
	@go build -o bin/gravity gravity.go

fmt:
	@find ./cipher ./cmd ./tunnel ./rpc ./protocol ./gnet -name '*.go'|xargs gofmt -w

test:
	@go test ./protocol/...

grpc: rpc/pb/gravity.proto
	@protoc rpc/pb/gravity.proto --go_out=plugins=grpc:.


INSTALLDIR=/usr/local
install:
	cp ./bin/galaxy ./bin/gravity $(INSTALLDIR)/bin
	mkdir -p $(INSTALLDIR)/etc/galaxy/certs
	test -e $(INSTALLDIR)/etc/galaxy/certs/server.key || openssl genrsa -out $(INSTALLDIR)/etc/galaxy/certs/server.key 4096
	test -e $(INSTALLDIR)/etc/galaxy/certs/server.pem || openssl req -x509 -key $(INSTALLDIR)/etc/galaxy/certs/server.key -out $(INSTALLDIR)/etc/galaxy/certs/server.pem
	test -e $(INSTALLDIR)/etc/galaxy/galaxy.yaml || echo "gravity:\n  addr: :12121\n  tls:\n    enabled: true\n    pem: /usr/local/etc/galaxy/certs/server.pem\n    key: /usr/local/etc/galaxy/certs/server.key" > $(INSTALLDIR)/etc/galaxy/galaxy.yaml

install-systemd:
	test -e /etc/systemd/system/galaxy.service || echo "[Unit]\nDescription=Galaxy\nAfter=network.target\n \n[Service]\nType=simple\nExecStart=$(INSTALLDIR)/bin/galaxy -c $(INSTALLDIR)/etc/galaxy/galaxy.yaml\nPrivateTmp=false\n \n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/galaxy.service

install-supervisor:
	test -e /etc/supervisor/conf.d/galaxy.conf || echo "[program:galaxy]\ncommand=$(INSTALLDIR)/bin/galaxy -c $(INSTALLDIR)/etc/galaxy/galaxy.yaml\ndirecotry=/tmp\nautostart=true\nstartsecs=3\nautorestart=true\nstdout_logfile_maxbytes=20MB\nstdout_logfile_backups=10\nstdout_logfile=/var/log/supervisor/galaxy.log\nstderr_logfile_maxbytes=5MB\nstderr_logfile_backups=10\nstderr_logfile=/var/log/supervisor/galaxy.err\n" > /etc/supervisor/conf.d/galaxy.conf

uninstall:
	rm -f $(INSTALLDIR)/bin/galaxy $(INSTALLDIR)/bin/gravity
	rm -rf $(INSTALLDIR)/etc/galaxy
	rm -f /etc/systemd/system/galaxy.service

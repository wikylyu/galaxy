/*
 * Copyright (C) 2018 Wiky Lyu
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.";
 */

package cmd

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

func FormatBytes(i uint64) string {
	if i < 1024 {
		return fmt.Sprintf("%dB", i)
	} else if i < 1024*1024 {
		return fmt.Sprintf("%.2fKB", float64(i)/1024.0)
	} else if i < 1024*1024*1024 {
		return fmt.Sprintf("%.2fMB", float64(i)/1024.0/1024.0)
	} else {
		return fmt.Sprintf("%.2fGB", float64(i)/1024.0/1024.0/1024.0)
	}
}

func GetIntSliceDefault(slice []int, i, def int) int {
	if len(slice) > i {
		return slice[i]
	}
	return def
}

/*  返回map的排序key */
func SortMap(m map[string]string) []string {
	keys := make([]string, 0)
	for k := range m {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return keys
}

/* 将1,2,3格式的字符串拆分成整数数组 */
func ExtractIntSlice(s string) ([]int, error) {
	ints := make([]int, 0)
	ff := func(c rune) bool { return c == ',' }
	for _, v := range strings.FieldsFunc(s, ff) {
		if i, err := strconv.Atoi(v); err != nil {
			return nil, err
		} else {
			ints = append(ints, i)
		}
	}
	return ints, nil
}
